import json
import os
from time import sleep
from loguru import logger as logging
import requests
import socket
from datetime import datetime, timedelta

config = {}

alertedIV = False
alertedBATV = False

lastIV = datetime.now() - timedelta(hours=1)
lastBATV = datetime.now() - timedelta(hours=1)


def raise_alert(zone: int):
    global config
    try:
        message = ""
        time_ = datetime.now().strftime("%H%M%S")
        date_ = datetime.now().strftime("%d%m%y")
        if zone == 1:
            message = f"""
            #1I,2,GAM1.2R,ATM/9568,0000{config['panel_no']},0000235070,0000235070,06-53-49-00-9E-6B,171526,091121,4294967295:0000000000,0000061440:0000000000,0000000036:0000000000,*,009,045,000,0000000292,0000000036,{time_},{date_},00002,00675,00673,000,000,UPS001,43,\n\r
            """
        else:
            message = f"""
            #1I,2,GAM1.2R,ATM/9568,0000{config['panel_no']},0000235070,0000235070,06-53-49-00-9E-6B,171526,091121,4294967295:0000000000,0000061440:0000000000,0000000036:0000000000,*,009,045,000,0000000292,0000000036,{time_},{date_},00002,00675,00673,000,000,UPS002,43,\n\r
            """
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
            s.connect((config['socket_ip'], config['socket_port']))
            s.sendall(message.encode())
            data = s.recv(1024)

    except Exception as e:
        logging.error(f'{e}')


def check_status():
    global config
    global alertedIV
    global alertedBATV
    global lastIV
    global lastBATV
    try:
        res = requests.get(f'http://{config["gateway_ip"]}:5001/devices/serial/listDevices')
        serial_devices = res.json()
        for singledevice in serial_devices:
            device_code = singledevice['device_code']

            res = requests.get(f'http://{config["gateway_ip"]}:5001/devices/serial/status?device_code={device_code}')

            inputs = res.json()['inputs']
            for singleinput in inputs:
                if singleinput['tags']['zone'] == 'IV' and singleinput['fields']['value'] <= config[
                    'iv_alert'] and not alertedIV and datetime.now() - lastIV >= timedelta(hours=1):
                    raise_alert(zone=1)
                    logging.info(f'Input Voltage 0 found for {device_code}')
                    print(f'Input Voltage 0 found for {device_code}')
                    alertedIV = True
                    lastIV = datetime.now()

                if singleinput['tags']['zone'] == 'BATV' and singleinput['fields']['value'] <= config[
                    'batv_alert'] and not alertedBATV and datetime.now() - lastBATV >= timedelta(hours=1):
                    raise_alert(zone=2)
                    logging.info(f'LOW BATV found for {device_code}')
                    print(f'LOW BATV found for {device_code}')
                    alertedBATV = True
                    lastBATV = datetime.now()

                if alertedIV and singleinput['tags']['zone'] == 'IV' and singleinput['fields']['value'] > config[
                    'iv_alert']:
                    # raise_alert(f'Input Voltage normal for {device_code}')
                    logging.info(f'Input Voltage normal for {device_code}')
                    alertedIV = False

                if alertedBATV and singleinput['tags']['zone'] == 'BATV' and singleinput['fields']['value'] > config[
                    'batv_alert']:
                    # raise_alert(f'BATV normal for {device_code}')
                    logging.info(f'BATV normal for {device_code}')
                    alertedBATV = False

    except Exception as e:
        logging.error(f'{e}')


if __name__ == '__main__':
    # global config
    with open(os.getenv('CONFIG_PATH')) as f:
        config = json.load(f)

    while True:
        check_status()
        # logging.info('now sleeping for 5 mins')
        sleep(10)
